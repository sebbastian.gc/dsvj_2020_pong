#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "globales.h"

using namespace std;
using namespace pong;
using namespace gamePlay;

namespace pong {

    namespace GamePlay {
        void inicializarJuego(Player& p1, Player& p2);
        void updateJuego(Player& p1, Player& p2, Pelota& bola);
        void updateJugador(Player& p1, Player& p2, Pelota& bola);
        void updateBola(Pelota& bola, Player& p1, Player& p2);
        void updateGraficos(Player& p1, Player& p2, Pelota& bola);
       
        void reiniciarbola(Pelota& bola, Player& p1, Player& p2);
        void reiniciarmultibola(Pelota& bola, Player& p1, Player& p2);
        void colisionPadBola(Player& pj, Player& p2, Pelota& bola);
        void colisionObstaculoBola(Rectangle& obs, Pelota& bola);
        void colisionPelota(Pelota& bola);       
        void moverPelota(Pelota& bola);
        void moverPelota(Pelota& bola, short dir);
        void moverPad(Player& pj);
        void moverPad(Player& pj, Pelota& bola);     
        void moverIA(Player& cpu, Pelota& bola);

        //opcines_juego------------------------------------------------------       
        void iniciarJuego(Pelota& bola, Player& p1, Player& p2);
        void victoria(Pelota& bola, Player& p1, Player& p2);
        void puntaje(Player& p1, Player& p2);
        void pause();
        void siguienteRonda(Player& p1, Player& p2, Pelota& bola);       
        void reinicarValores(Player& p1, Player& p2, Pelota& bola);
        void reiniciarJuego(Player& p1, Player& p2, Pelota& bola);         

        void dibIniciarJuego(Pelota& bola, Player& p1, Player& p2);
        void dibPause();
        void dibVictoria(Pelota& bola, Player& p1, Player& p2);
    }
}
#endif
