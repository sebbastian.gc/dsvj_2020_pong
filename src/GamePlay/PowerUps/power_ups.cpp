#include"power_ups.h"

#include "GamePlay/GamePlay.h"

using namespace GamePlay;
using namespace powerUps;

bool powerUps::powerUpTurno;
bool powerUps::powerUpTiempo;
bool powerUps::inverVelocidad;
float powerUps::tiempo;
POWERUPSTURNO powerUps::powerRandTurno;
POWERUPSTIEMPO powerUps::powerRandTiempo;
int powerUps::arriba;
int powerUps::abajo;
Pelota powerUps::bola1;
Pelota powerUps::bola2;
Rectangle powerUps::obstaculo1;
Rectangle powerUps::obstaculo2;
Rectangle powerUps::scudo;

void powerUps::inicializarPowerUps() {
	 powerUps::powerUpTurno = false;
	 powerUps::powerUpTiempo = false;
	 powerUps::inverVelocidad = false;
	 powerUps::tiempo = 0.0f;	
	 powerUps::arriba = 0;
	 powerUps::abajo = 0;
	 powerUps::bola1;
	 powerUps::bola2;	 
}
void powerUps::updatePowerUps(Player& p1, Player& p2, Pelota& bola) {
	powerUps::powerupsTurno(p1, p2, bola);
	powerUps::powerupsTiempo(p1, p2, bola);
}
void powerUps::updateGraficos(Player& p1, Pelota& bola, Player& p2) {
	powerUps::dibPowerupTurnoActivado(p1, p2);
	powerUps::dibPowerupTiempoActivado();
	powerUps::dibEscudo(p1, bola,p2);
	powerUps::dibBolas();
	powerUps::dibObstaculos();
}
//hasta el proximo turno 
//-----------------------------------------------------------------------------------------------------
void powerUps::escudo(Player &p1, Pelota& bola) {
	if (p1.powerActivo) {
		powerUps::crearEscudo(powerUps::scudo, 12, static_cast<int>(screenHeight / 22.5f), bola);
	}
	else  powerUps::crearEscudo(powerUps::scudo, static_cast<int>(screenWidth -20), static_cast<int>(screenHeight / 22.5f), bola);
}
void powerUps::agrandarPad(Player &pj) {
	pj.pad.height = 110;	
}
void powerUps::aumentarVelocidad(Player& pj) {
	pj.velocidad = 7.3f;
}
void powerUps::multiBola(Player& p1, Player& p2) {
	if (powerUps::powerUpTurno) {
		powerUps::crearBolas(powerUps::bola1, -1, p1, p2);
		powerUps::crearBolas(powerUps::bola2, 1, p1, p2);		
	}
}
void powerUps::crearEscudo(Rectangle& escudo, int posx, int posy, Pelota& bola) {
	escudo.width = 10;
	escudo.height = 410;
	escudo.x = static_cast<float>(posx);
	escudo.y = static_cast<float>(posy);
	colisionObstaculoBola(escudo, bola);
	colisionObstaculoBola(escudo, powerUps::bola1);
	colisionObstaculoBola(escudo, powerUps::bola2);	
}
// por x tiempo 
//-----------------------------------------------------------------------------------------------------
bool powerUps::timer(int tiempo) {
	powerUps::tiempo += 0.17f;
	if (powerUps::tiempo >= tiempo) {
		powerUps::tiempo = 0;
		return true;
	}
	else return false;	  
}
void powerUps::invertirVelocidad() {
	if (powerUps::powerUpTiempo) {		
		powerUps::inverVelocidad = true;
		if (powerUps::timer(250)){
			powerUps::powerUpTiempo = false;
			powerUps::inverVelocidad = false;
		}
	}
}
void powerUps::obstaculos(Pelota& bola) {
	if (powerUps::powerUpTiempo) {		
		powerUps::crearObstaculos(powerUps::obstaculo1, static_cast<int>(screenWidth / 2.105f), static_cast<int>(screenHeight / 1.5f), bola);
		powerUps::crearObstaculos(powerUps::obstaculo2, static_cast<int>(screenWidth / 2.105f), static_cast<int>(screenHeight / 5.63f), bola);
		if (powerUps::timer(250)) {
			powerUps::powerUpTiempo = false;
		}
	}
}
void powerUps::reset(Player& pj) {
	if (!powerUps::powerUpTiempo) {
		powerUps::arriba = pj.arriba;
		powerUps::abajo = pj.abajo;
	}	
}
void powerUps::invertirControles(Player& pj) {
	if (powerUps::powerUpTiempo) {		
		pj.arriba = powerUps::abajo;
		pj.abajo = powerUps::arriba;

		if (powerUps::timer(250)) {
			pj.arriba = powerUps::arriba;
			pj.abajo = powerUps::abajo;
			powerUps::powerUpTiempo = false;
		}
	}
}
void powerUps::crearObstaculos(Rectangle& obs,int posx, int posy ,Pelota& bola) {
	obs.width = 40;
	obs.height = 80;
	obs.x = static_cast<float>(posx);
	obs.y = static_cast<float>(posy);
	colisionObstaculoBola(obs, bola);	
}
void powerUps::crearBolas(Pelota& bola,short dir, Player& p1, Player& p2) {
	bola.reinicio = false;
	moverPelota(bola,dir);	
	colisionPadBola(p1, p2, bola);	
	reiniciarmultibola(bola, p1, p2);		
}
//-----------------------------------------------------------------------------------------------------
void powerUps::powerupsTiempo(Player& p1, Player& p2, Pelota &bola) {
	if (!gamePlay::pausa) {
		if (!bola.reinicio) {
			if (powerUps::timer(300)) {
				int aux = rand() % (4);
				powerUps::powerRandTiempo =(POWERUPSTIEMPO) aux;
				if (powerUps::powerRandTiempo == POWERUPSTIEMPO::INVERTIRCONTROLES1) { powerUps::reset(p1); }
				else if (powerUps::powerRandTiempo == POWERUPSTIEMPO::INVERTIRCONTROLES2) { powerUps::reset(p2); }
				powerUps::powerUpTiempo = true;					
			}				
			if (powerUps::powerRandTiempo == POWERUPSTIEMPO::INVERTIRVELOCIDAD) { powerUps::invertirVelocidad(); }			  
			else if (powerUps::powerRandTiempo == POWERUPSTIEMPO::OBSTACULOS) { powerUps::obstaculos(bola); }
			else if (powerUps::powerRandTiempo == POWERUPSTIEMPO::INVERTIRCONTROLES1) { powerUps::invertirControles(p1); }
			else if (powerUps::powerRandTiempo == POWERUPSTIEMPO::INVERTIRCONTROLES2) { powerUps::invertirControles(p2); }
		}			
	}
}
void powerUps::powerupsTurno(Player& p1, Player& p2, Pelota& bola) {
	powerUps::restaurarValores(p1);
	powerUps::restaurarValores(p2);	
	if (!powerUps::powerUpTurno){
		powerUps::reiniciarBola();
		int aux =  rand() % (4);
		powerUps::powerRandTurno = (POWERUPSTURNO) aux;
		
	}
	if (powerUps::powerUpTurno) {
		if (p1.powerActivo) {
			switch (powerUps::powerRandTurno){	
				case POWERUPSTURNO::AGRANDARPAD:
					powerUps::agrandarPad(p1);
					break;
				case POWERUPSTURNO::AUMENTARVELOCIDAD:
					powerUps::aumentarVelocidad(p1);
					break;
				case POWERUPSTURNO::ESCUDO:
					powerUps::escudo(p1, bola);
					break;
				case POWERUPSTURNO::MULTIBOLA:
					if (!gamePlay::pausa) {
						if (!bola.reinicio) {
							powerUps::multiBola(p1, p2);
						}
					}
					break;
		    }		
		}
		else if (p2.powerActivo) {			
			switch (powerUps::powerRandTurno){
				case POWERUPSTURNO::AGRANDARPAD:
					powerUps::agrandarPad(p2);
					break;
				case POWERUPSTURNO::AUMENTARVELOCIDAD:
					powerUps::aumentarVelocidad(p2);
					break;
				case POWERUPSTURNO::ESCUDO:
					powerUps::escudo(p1, bola);
					break;
				case POWERUPSTURNO::MULTIBOLA:
					if (!gamePlay::pausa) {
						if (!bola.reinicio) {
							powerUps::multiBola(p1, p2);
						}
					}
					break;
			}
		}
	}	
}
void powerUps::restaurarValores(Player& pj) {

	pj.pad.height = 80;
	pj.velocidad = 4.0f;
	if (pj.player == 2 && gamePlay::IA) { pj.velocidad = 7.3f; }
}

void powerUps::dibEscudo(Player& p1, Pelota& bola, Player& p2) {
	if (powerUps::powerUpTurno) {
		if (p1.powerActivo) {
			DrawRectangleRec(powerUps::scudo, p1.tono);
		}
		else if (p2.powerActivo) {
			DrawRectangleRec(powerUps::scudo, p2.tono);
		}
	}	
}
void powerUps::dibPowerupTurnoActivado(Player& p1, Player& p2) {
	if (powerUps::powerUpTurno) {
		if (p1.powerActivo) {
			switch (powerUps::powerRandTurno) {
				case POWERUPSTURNO::AGRANDARPAD:
					DrawText("Agrandar", screenWidth / 5, screenHeight / 45, menu::tamañoFuente , YELLOW);
					break;
				case POWERUPSTURNO::AUMENTARVELOCIDAD:
					DrawText("Velocidad", screenWidth / 5, screenHeight / 45, menu::tamañoFuente , YELLOW);
					break;
				case POWERUPSTURNO::ESCUDO:
					DrawText("Escudo", screenWidth / 5, screenHeight / 45, menu::tamañoFuente , YELLOW);
					break;
				case POWERUPSTURNO::MULTIBOLA:
					DrawText("Multibola", screenWidth / 5, screenHeight / 45, menu::tamañoFuente, YELLOW);
					break;
			}
		}
		else if (p2.powerActivo) {
			switch (powerUps::powerRandTurno) {
				case POWERUPSTURNO::AGRANDARPAD:
					DrawText("Agrandar", static_cast<int>(screenWidth / 1.5f), screenHeight / 45, menu::tamañoFuente , YELLOW);
					break;				
				case POWERUPSTURNO::AUMENTARVELOCIDAD:
					DrawText("Velocidad", static_cast<int>(screenWidth / 1.5f), screenHeight / 45, menu::tamañoFuente , YELLOW);
					break;
				case POWERUPSTURNO::ESCUDO:
					DrawText("Escudo", static_cast<int>(screenWidth / 1.5f), screenHeight / 45, menu::tamañoFuente , YELLOW);
					break;
				case POWERUPSTURNO::MULTIBOLA:
					DrawText("Multibola", static_cast<int>(screenWidth / 1.5f), screenHeight / 45, menu::tamañoFuente, YELLOW);
					break;
			}
		}
	}
}
void powerUps::dibPowerupTiempoActivado(Player& p1, Player& p2) {
	if (powerUps::powerUpTiempo) {
		DrawRectangle(screenWidth / 2, screenHeight / 45, 10, 20, YELLOW);
	}
}
void powerUps::dibBolas() {
	if (powerUps::powerUpTurno && powerUps::powerRandTurno == POWERUPSTURNO::MULTIBOLA) {
		DrawCircleV(powerUps::bola1.pos, powerUps::bola1.radio, GREEN);
		DrawCircleV(powerUps::bola2.pos, powerUps::bola2.radio, PINK);
	}
}
void powerUps::dibObstaculos() {
	if (powerUps::powerUpTiempo && powerUps::powerRandTiempo == POWERUPSTIEMPO::OBSTACULOS) {
		DrawRectangleRec(powerUps::obstaculo1, RED);
		DrawRectangleRec(powerUps::obstaculo2, RED);
	}
}
void powerUps::reiniciarBola() {
	powerUps::bola1.pos= { static_cast<float>(screenWidth) / 2, static_cast<float>(screenHeight) / 2 };
	powerUps::bola2.pos = { static_cast<float>(screenWidth) / 2, static_cast<float>(screenHeight) / 2 };
}

void powerUps::dibPowerupTiempoActivado() {
	if (powerUps::powerUpTiempo) {
		DrawRectangle(screenWidth / 2, screenHeight / 45, 10, 20, YELLOW);
	}
}


