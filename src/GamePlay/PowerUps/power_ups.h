#ifndef POWERUPS_H
#define POWERUPS_H

#include "GamePlay/globales.h"

using namespace pong;
using namespace gamePlay;

namespace pong {

	namespace powerUps {
		void inicializarPowerUps();
		void updatePowerUps(Player& p1, Player& p2, Pelota& bola);
		void updateGraficos(Player& p1, Pelota& bola, Player& p2);

		void escudo(Player& p1, Pelota& bola);
		void agrandarPad(Player& pj);
		void aumentarVelocidad(Player& pj);
		void multiBola(Player& p1, Player& p2);
		void crearEscudo(Rectangle& escudo, int posx, int posy, Pelota& bola);
		
		bool timer(int tiempo);
		void invertirVelocidad();		
		void obstaculos(Pelota& bola);	
		void reset(Player& pj);
		void invertirControles(Player& pj);		
		void crearObstaculos(Rectangle& obs, int posx, int posy, Pelota& bola);
		void crearBolas(Pelota& bola, short dir, Player& p1, Player& p2);		

		void powerupsTiempo(Player& p1, Player& p2, Pelota& bola);
		void powerupsTurno(Player& p1, Player& p2, Pelota& bola);
		void reiniciarBola();
		void restaurarValores(Player& pj);

		void dibEscudo(Player& p1, Pelota& bola, Player& p2);
		void dibPowerupTurnoActivado(Player& p1, Player& p2);
		void dibPowerupTiempoActivado(Player& p1, Player& p2);
		void dibBolas();
		void dibObstaculos();
		void dibPowerupTiempoActivado();
	}
}
#endif