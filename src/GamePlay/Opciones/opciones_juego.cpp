#include"GamePlay/gameplay.h"

#include"GamePlay/PowerUps/power_ups.h"

void GamePlay::iniciarJuego(Pelota& bola, Player& p1, Player& p2) {
    if (bola.reinicio && !p1.victoria && !p2.victoria) {
        if (IsKeyPressed(KEY_ENTER)) { bola.reinicio = false; }
    }
}
void GamePlay::victoria(Pelota& bola, Player& p1, Player& p2) {
    if (p1.victoria) {
        bola.reinicio = true;       
        reiniciar = true;
    }
    else if (p2.victoria) {
        bola.reinicio = true;        
        reiniciar = true;
    }
}
void GamePlay::puntaje(Player& p1, Player& p2) {   
    DrawText(TextFormat("%01i", p1.puntaje), static_cast<int>(screenWidth / 2.145f), screenHeight / 45, menu::tamañoFuente+10, RED);
    DrawText(TextFormat("%01i", p2.puntaje), static_cast<int>(screenWidth / 1.905f), screenHeight / 45, menu::tamañoFuente+10, RED);
    DrawText(TextFormat("%01i", rondaActual), static_cast<int>(screenWidth / 1.99f), static_cast<int>(screenHeight / 11.25f), menu::tamañoFuente-5, YELLOW);
    DrawText(TextFormat("%01i", p1.puntRonda), screenWidth / 10, screenHeight / 45, menu::tamañoFuente+10, YELLOW);
    DrawText(TextFormat("%01i", p2.puntRonda), static_cast<int>(screenWidth / 1.142f), screenHeight / 45, menu::tamañoFuente+10, YELLOW);
}
void GamePlay::pause() {
    if (IsKeyPressed(KEY_SPACE)) {
        gamePlay::pausa ? gamePlay::pausa = false : gamePlay::pausa = true;
    }
    if (IsKeyPressed(KEY_BACKSPACE)&& gamePlay::pausa) {
        gamePlay::reiniciar = true;
    }   
}
void GamePlay::siguienteRonda(Player& p1, Player& p2, Pelota& bola) {

    if (IsKeyPressed(KEY_ENTER) && (p1.victoria || p2.victoria)) {
        p1.victoria ? p1.puntRonda++ : p2.puntRonda++;
        gamePlay::sigRonda = true;
        gamePlay::rondaActual++;
        GamePlay::reinicarValores(p1, p2, bola);
    }
}
void GamePlay::reinicarValores(Player& p1, Player& p2, Pelota& bola) {
    p1.pad.x = 0;
    p1.pad.y = static_cast<float>(gamePlay::screenHeight) / 2 - p1.pad.height / 2;
    p1.puntaje = 0;
    p1.pad.height = 80;
    p1.velocidad = 4.0f;
    p1.victoria = false;
    p1.powerActivo = false;
    p1.tono = RED;
    if (!gamePlay::sigRonda) { p1.puntRonda = 0; }
    p1.arriba = (int)KeyboardKey::KEY_A;
    p1.abajo = (int)KeyboardKey::KEY_D;

    p2.pad.x = static_cast<float>(gamePlay::screenWidth) - p2.pad.width;
    p2.pad.y = static_cast<float>(gamePlay::screenHeight) / 2 - p2.pad.height / 2;
    p2.pad.height = 80;
    p2.puntaje = 0;
    p2.victoria = false;
    p2.powerActivo = false;
    p2.velocidad = 4.0f;
    p2.tono = RED;
    if (!gamePlay::sigRonda) { p2.puntRonda = 0; }
    p2.arriba = (int)KeyboardKey::KEY_UP;
    p2.abajo = (int)KeyboardKey::KEY_DOWN;

    gamePlay::vidasEscudo = 2;
    bola.pos.x = static_cast<float>(gamePlay::screenWidth) / 2;
    bola.pos.y = static_cast<float>(gamePlay::screenHeight) / 2;
    bola.reinicio = true;
    bola.tono = ORANGE;

    gamePlay::pausa = false;
    gamePlay::reiniciar = false;
    powerUps::powerUpTiempo = false;
    powerUps::powerUpTurno = false;
    if (!gamePlay::sigRonda) { rondaActual = 1; }
    gamePlay::sigRonda = false;
}
void GamePlay::reiniciarJuego(Player& p1, Player& p2, Pelota& bola) {
    if (IsKeyDown(KEY_BACKSPACE) && reiniciar) {
        GamePlay::reinicarValores(p1, p2, bola);
        GameLoop::pantallaActual = PANTALLAS::MENU;
    }
}

void GamePlay::dibIniciarJuego(Pelota& bola, Player& p1, Player& p2) {
    if (bola.reinicio && !p1.victoria && !p2.victoria) {
        DrawText("press enter to play ", static_cast<int>(screenWidth / 3.2f), screenHeight/3, menu::tamañoFuente, GREEN);
    }
}
void GamePlay::dibPause() {
    if (gamePlay::pausa) {
        DrawText("press space to play ", static_cast<int>(screenWidth / 3.2f), screenHeight / 3, menu::tamañoFuente, GREEN);
        DrawText("press backspace to extir the menu ", static_cast<int>(screenWidth / 5.33), static_cast<int>(screenHeight / 1.8f), menu::tamañoFuente, GREEN);
    }
}
void GamePlay::dibVictoria(Pelota& bola, Player& p1, Player& p2) {
    if (p1.victoria) {
        DrawText("player 1 wins ", static_cast<int>(screenWidth / 2.667f), screenHeight / 3, menu::tamañoFuente, GREEN);
        DrawText("press backspace to exit the menu ", static_cast<int>(screenWidth / 4.71f), static_cast<int>(screenHeight / 1.5f), menu::tamañoFuente, GREEN);
        DrawText("press enter to to play again", static_cast<int>(screenWidth / 4.71f), static_cast<int>(screenHeight / 1.255f), menu::tamañoFuente, GREEN);
    }
    else if (p2.victoria) {
        DrawText("player 2 wins ", static_cast<int>(screenWidth / 2.667f), screenHeight / 3, menu::tamañoFuente, GREEN);
        DrawText("press backspace to exit the menu ", static_cast<int>(screenWidth / 4.71f), static_cast<int>(screenHeight / 1.5f), menu::tamañoFuente, GREEN);
        DrawText("press enter to to play again ", static_cast<int>(screenWidth / 4.71f), static_cast<int>(screenHeight / 1.255f), menu::tamañoFuente, GREEN);
    }
}
