#ifndef GLOBALES_H
#define GLOBALES_H

#include <iostream>
#include <stdlib.h>
#include <time.h>

#include "raylib.h"

namespace pong {

    namespace gamePlay {

        extern const int screenWidth;
        extern const int screenHeight;
        extern short puntajeVictoria;
        extern bool pausa;
        extern bool reiniciar;
        extern short rondaActual;
        extern bool sigRonda;
        extern bool IA;
        extern short vidasEscudo;

        enum class PANTALLAS { MENU, OPCJUGAR, OPCIONES, CREDITOS, SALIR, GAMEPLAY, OPCIONESCOLOR,INSTRUCCIONES };

        struct Player {
            Rectangle pad = {0,0,0,0};
            float velocidad = 4.0f;
            short player = 0;
            short puntaje = 0;
            short puntRonda = 0;
            bool victoria = false;
            bool powerActivo = false;
            int  arriba = 0;
            int  abajo = 0;
            Color tono = RED;
        };

        struct Pelota {
            Vector2 pos = { static_cast<float>(screenWidth) / 2, static_cast<float>(screenHeight) / 2 };
            Vector2 velocidad = { 5.0f, 5.0f };
            float radio = 20.0f;
            bool colision = false;
            bool reinicio = true;
            bool colisionPad = false;
            Color tono = ORANGE;
        };
    }
    namespace menu {
        extern short i;
        extern short j;
        extern short p;
        extern bool p1CambioArriba;
        extern bool p1CambioAbajo;
        extern bool p2CambioArriba;
        extern bool p2CambioAbajo;
        extern short tamañoFuente;
    }

    namespace GameLoop {
        extern gamePlay::PANTALLAS pantallaActual;
        extern gamePlay::Player P1;
        extern gamePlay::Player P2;
        extern gamePlay::Pelota bola;
    }
    namespace powerUps {

        enum class POWERUPSTIEMPO { INVERTIRVELOCIDAD, OBSTACULOS, INVERTIRCONTROLES1, INVERTIRCONTROLES2 };
        enum class POWERUPSTURNO { AGRANDARPAD, AUMENTARVELOCIDAD, ESCUDO, MULTIBOLA };

        extern bool powerUpTurno;
        extern bool powerUpTiempo;
        extern bool inverVelocidad;
        extern gamePlay::Pelota bola1;
        extern gamePlay::Pelota bola2;
        extern Rectangle obstaculo1;
        extern Rectangle obstaculo2;
        extern Rectangle scudo;
        extern POWERUPSTURNO powerRandTurno;
        extern POWERUPSTIEMPO powerRandTiempo;
        extern int arriba;
        extern int abajo;
        extern float tiempo;

        
    }
}




#endif