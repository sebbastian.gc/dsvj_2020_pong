#include"Gameplay.h"

#include"PowerUps/power_ups.h"

using namespace GamePlay;

const int gamePlay::screenWidth = 800;
const int gamePlay::screenHeight = 450;
short gamePlay::puntajeVictoria;
bool  gamePlay::pausa;
bool  gamePlay::reiniciar;
short gamePlay::rondaActual;
bool  gamePlay::sigRonda;
bool  gamePlay::IA;
short gamePlay::vidasEscudo;

void GamePlay::inicializarJuego(Player& p1, Player& p2) {
    gamePlay::puntajeVictoria = 6;
    gamePlay::vidasEscudo = 2;
    gamePlay::pausa = false;
    gamePlay::reiniciar = false;
    gamePlay::rondaActual = 1;
    gamePlay::sigRonda = false;
    gamePlay::IA = false;

    GameLoop::pantallaActual = PANTALLAS::MENU;
    p1.player = 1;
    p1.pad.width = 20;
    p1.pad.height = 80;
    p1.pad.x = 30;
    p1.pad.y = static_cast<float>(gamePlay::screenHeight) / 2 - p1.pad.height / 2;
    p1.arriba = (int)KeyboardKey::KEY_A;
    p1.abajo = (int)KeyboardKey::KEY_D;

    p2.player = 2;
    p2.pad.width = 20;
    p2.pad.height = 80;
    p2.pad.x = static_cast<float>(gamePlay::screenWidth)-30 - p2.pad.width;
    p2.pad.y = static_cast<float>(gamePlay::screenHeight) / 2 - p2.pad.height / 2;
    p2.arriba = (int)KeyboardKey::KEY_UP;
    p2.abajo = (int)KeyboardKey::KEY_DOWN;
}
void GamePlay::updateJuego(Player& p1, Player& p2, Pelota& bola) {
    GamePlay::updateJugador(p1, p2, bola);
    GamePlay::updateBola(bola, p1, p2);
    GamePlay::reiniciarJuego(p1, p2, bola);
}
void GamePlay::updateJugador(Player& p1, Player& p2, Pelota& bola) {
    p1.pad.x = 30;
    p2.pad.x = static_cast<float>(gamePlay::screenWidth)-30 - p2.pad.width;
    GamePlay::iniciarJuego(bola, p1, p2);
    if (!bola.reinicio) { GamePlay::pause(); }
    if (!gamePlay::pausa) {
        if (!bola.reinicio) {
            GamePlay::moverPad(p1);
            GamePlay::moverPad(p2,bola);
        }
        GamePlay::colisionPadBola(p1,p2, bola);
    }         
    GamePlay::victoria(bola, p1, p2);        
}
void GamePlay::updateBola(Pelota& bola , Player& p1, Player& p2) {
    if (!gamePlay::pausa) {
        if (!bola.reinicio) {
            GamePlay::moverPelota(bola);
            GamePlay::reiniciarbola(bola, p1, p2);
        }
    }     
}
void GamePlay::updateGraficos(Player& p1, Player& p2, Pelota& bola) {   
    GamePlay::dibVictoria(bola, p1, p2);
    DrawRectangleRec(p1.pad, p1.tono);
    DrawRectangleRec(p2.pad, p2.tono);
    DrawCircleV(bola.pos, bola.radio, bola.tono);
    GamePlay::dibIniciarJuego(bola, p1, p2);
    GamePlay::puntaje(p1, p2);
    if (!bola.reinicio) { GamePlay::dibPause(); }
    GamePlay::siguienteRonda(p1, p2, bola);     
}

void GamePlay::reiniciarbola(Pelota& bola, Player& p1, Player& p2) {

    if (bola.pos.x > static_cast<float>(gamePlay::screenWidth) + bola.radio) {
        bola.pos = { static_cast<float>(gamePlay::screenWidth) / 2, static_cast<float>(gamePlay::screenHeight) / 2 };
        bola.velocidad.x *= -1.0f;
        bola.reinicio = true;
        p1.puntaje++;
        p2.powerActivo = false;
        p1.powerActivo = true;
        bola.colisionPad = false;
        powerUps::powerUpTurno ? powerUps::powerUpTurno =false:powerUps::powerUpTurno = true;
        if (p1.puntaje == gamePlay::puntajeVictoria) { p1.victoria = true; }
    }
    else if (bola.pos.x < -bola.radio) {
        bola.pos = { static_cast<float>(gamePlay::screenWidth) / 2,  static_cast<float>(gamePlay::screenHeight) / 2 };
        bola.velocidad.x *= -1.0f;
        bola.reinicio = true;
        p2.puntaje++;
        p1.powerActivo = false;
        p2.powerActivo = true;
        bola.colisionPad = false;
        powerUps::powerUpTurno ? powerUps::powerUpTurno = false : powerUps::powerUpTurno = true;
        if (p2.puntaje == gamePlay::puntajeVictoria) { p2.victoria = true; }
    }
}
void GamePlay::reiniciarmultibola(Pelota& bola, Player& p1, Player& p2) {
    if (bola.pos.x > static_cast<float>(gamePlay::screenWidth) + bola.radio) {
        bola.pos = { static_cast<float>(gamePlay::screenWidth) / 2, static_cast<float>(gamePlay::screenHeight) / 2 };
        bola.velocidad.x *= -1.0f;
        bola.reinicio = true;
        bola.colisionPad = false;
        p1.puntaje++;      
        if (p1.puntaje == gamePlay::puntajeVictoria) { p1.victoria = true; }
    }
    else if (bola.pos.x < -bola.radio) {
        bola.pos = { static_cast<float>(gamePlay::screenWidth) / 2, static_cast<float>(gamePlay::screenHeight) / 2 };
        bola.velocidad.x *= -1.0f;
        bola.reinicio = true;
        bola.colisionPad = false;
        p2.puntaje++;        
        if (p2.puntaje == gamePlay::puntajeVictoria) { p2.victoria = true; }
    }

}
void GamePlay::colisionPadBola(Player& p1, Player& p2, Pelota& bola) {

    if (CheckCollisionCircleRec(bola.pos, bola.radio, p1.pad)&& !bola.colision && !bola.colisionPad &&bola.velocidad.x<0) {
        if (bola.pos.x + bola.radio >p1.pad.x + p1.pad.width){
            bola.velocidad.x *= -1.0f;
            if (powerUps::powerUpTiempo && powerUps::inverVelocidad) { bola.velocidad.y *= -1.0f; }
        }
        else{
            bola.velocidad.y *= -1.0f;
            bola.colisionPad = true;
        }
        bola.colision = true;
    }
    else if (CheckCollisionCircleRec(bola.pos, bola.radio, p2.pad) && !bola.colision && !bola.colisionPad && bola.velocidad.x > 0) {
         if (bola.pos.x - bola.radio < p2.pad.x ){
            bola.velocidad.x *= -1.0f;
            if (powerUps::powerUpTiempo && powerUps::inverVelocidad) { bola.velocidad.y *= -1.0f; }
         }
         else{
            bola.velocidad.y *= -1.0f;
            bola.colisionPad = true;
         }
         bola.colision = true;
    }   
}
void GamePlay::colisionObstaculoBola(Rectangle& obs, Pelota& bola) {
    if (CheckCollisionCircleRec(bola.pos, bola.radio, obs) && !bola.colision &&!bola.colisionPad && bola.velocidad.x < 0) {
        if (bola.pos.x + bola.radio  > obs.x + obs.width) {
            bola.velocidad.x *= -1.0f;           
        }
        else {
            bola.velocidad.y *= -1.0f;
            bola.colisionPad = true;
        }        
        bola.colision = true;
        if (powerUps::powerUpTurno ) {
            gamePlay::vidasEscudo--;
            if (gamePlay::vidasEscudo <= 0) {
                powerUps::powerUpTurno = false;
                gamePlay::vidasEscudo = 2;
            }
        }            
    }
    else if (CheckCollisionCircleRec(bola.pos, bola.radio, obs) && !bola.colision && !bola.colisionPad && bola.velocidad.x > 0) {
        if (bola.pos.x - bola.radio < obs.x) {
            bola.velocidad.x *= -1.0f;            
        }
        else {
            bola.velocidad.y *= -1.0f;
            bola.colisionPad = true;
        }
        bola.colision = true;

        if (powerUps::powerUpTurno) {
            gamePlay::vidasEscudo--;
            if (gamePlay::vidasEscudo <= 0) {
                powerUps::powerUpTurno = false;
                gamePlay::vidasEscudo=2;
            }
        }
    }
}
void GamePlay::colisionPelota(Pelota& bola) {

    if ((bola.pos.y >= (static_cast<float>(gamePlay::screenHeight) - bola.radio)) || (bola.pos.y <= bola.radio)) {
        bola.velocidad.y *= -1.0f;
        bola.colisionPad = false;
    }
}
void GamePlay::moverPelota(Pelota& bola) {
    if (!bola.reinicio) {
        bola.colision = false;
        bola.pos.y += bola.velocidad.y;
        bola.pos.x += bola.velocidad.x;
        GamePlay::colisionPelota(bola);
    }
}
void GamePlay::moverPelota(Pelota& bola,short dir) {
    if (!bola.reinicio) {
        bola.colision = false;
        bola.pos.y += bola.velocidad.y * dir;
        bola.pos.x += bola.velocidad.x ;
        GamePlay::colisionPelota(bola);
    }
}
void GamePlay::moverPad(Player& pj) {
   
    if (IsKeyDown((KeyboardKey)pj.abajo) && pj.pad.y + pj.pad.height < static_cast<float>(gamePlay::screenHeight)) pj.pad.y += pj.velocidad;
    if (IsKeyDown((KeyboardKey)pj.arriba) && pj.pad.y > 0) pj.pad.y -= pj.velocidad;
}
void GamePlay::moverPad(Player& pj, Pelota& bola) {
  if (gamePlay::IA) {
      GamePlay::moverIA(pj,bola);
  }
  else {
      if (IsKeyDown((KeyboardKey)pj.abajo) && pj.pad.y + pj.pad.height < static_cast<float>(gamePlay::screenHeight)) pj.pad.y += pj.velocidad;
      if (IsKeyDown((KeyboardKey)pj.arriba) && pj.pad.y > 0) pj.pad.y -= pj.velocidad;
  }    
}
void GamePlay::moverIA(Player& cpu, Pelota& bola) {
    cpu.velocidad = 7.3f;

    if (bola.pos.y <  static_cast<float>(gamePlay::screenHeight) && bola.pos.y > static_cast<float>(gamePlay::screenHeight) / 2 && cpu.pad.y + cpu.pad.height < static_cast<float>(gamePlay::screenHeight) -30) {
        cpu.pad.y += cpu.velocidad; 
    }
    if (bola.pos.y> 0 && bola.pos.y< static_cast<float>(gamePlay::screenHeight) / 2 && cpu.pad.y >30) {
        cpu.pad.y -= cpu.velocidad; 
    }
}

