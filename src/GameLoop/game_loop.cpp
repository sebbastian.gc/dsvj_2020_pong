#include"GameLoop/game_loop.h"

#include "GamePlay/Gameplay.h"
#include "GamePlay/PowerUps/power_ups.h"
#include "Menu/menu.h"

using namespace pong;

PANTALLAS GameLoop::pantallaActual;
Player GameLoop::P1;
Player GameLoop::P2;
Pelota GameLoop::bola;

void GameLoop::inicializar() {
    srand(static_cast<unsigned int>(time(NULL)));
	GameLoop::pantallaActual = PANTALLAS::MENU;
    menu::inicializarMenu();
    GamePlay::inicializarJuego(P1, P2);
    powerUps::inicializarPowerUps();    
    InitWindow(gamePlay::screenWidth, gamePlay::screenHeight, " ");
    SetTargetFPS(60);
}
void GameLoop::ejecutar() {
    GameLoop::inicializar();
    while (!WindowShouldClose()){
        GameLoop::actualizar();
        GameLoop::dibujar();
    }

}
void GameLoop::actualizar() {
	switch (GameLoop::pantallaActual) {
		case PANTALLAS::MENU:
				menu::pantallaMenu();
				break;
		case PANTALLAS::OPCJUGAR:
				menu::pantallaJugar();
				break;
		case PANTALLAS::INSTRUCCIONES:
				menu::pantallaInstrucciones();
				break;
		case PANTALLAS::GAMEPLAY:
				GamePlay::updateJuego(P1, P2, bola);
				powerUps::updatePowerUps(P1, P2, bola);
				break;
		case PANTALLAS::OPCIONESCOLOR:
				menu::pantallaOpColor(P1, P2,bola);
				break;
		case PANTALLAS::OPCIONES:
				menu::pantallaControles(P1, P2);
				break;
		case PANTALLAS::CREDITOS:
				menu::pantallaCreditos();
				break;
		case PANTALLAS::SALIR:
				CloseWindow();
				break;
	}    
}
void GameLoop::dibujar() {
	BeginDrawing();
	ClearBackground(BLACK);
	switch (GameLoop::pantallaActual) {
		case PANTALLAS::MENU:
				menu::dibPantallaMenu();
				break;
		case PANTALLAS::OPCJUGAR:
				menu::dibPantallaJugar();
				break;
		case PANTALLAS::INSTRUCCIONES:
				menu::dibPantallaInstrucciones();
				break;
		case PANTALLAS::GAMEPLAY:
				powerUps::updateGraficos(P1,bola,P2);
				GamePlay::updateGraficos(P1, P2, bola);
				break;
		case PANTALLAS::OPCIONESCOLOR:
				menu::dibPantallaOpColor(P1, P2,bola);
				break;
		case PANTALLAS::OPCIONES:
				menu::dibPantallaControles(P1, P2);
				break;
		case PANTALLAS::CREDITOS:
				menu::dibPantallaCreditos();
				break;		
	}
	EndDrawing();    
}
