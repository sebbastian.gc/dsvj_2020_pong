#ifndef GAMELOOP_H
#define GAMELOOP_H

#include "GamePlay/globales.h"

namespace pong {

	namespace GameLoop {

		void inicializar();
		void ejecutar();
		void actualizar();
		void dibujar();
	}
}

#endif
