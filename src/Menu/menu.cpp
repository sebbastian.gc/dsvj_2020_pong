#include"menu.h"

using namespace menu;
using namespace gamePlay;

short menu::i;
short menu::j;
short menu::p;
bool menu::p1CambioArriba;
bool menu::p1CambioAbajo;
bool menu::p2CambioArriba;
bool menu::p2CambioAbajo;
short menu::tamañoFuente;
 

void menu::inicializarMenu() {
	 menu::i = 0;
	 menu::j = 0;
	 menu::p = 0;
	 menu::p1CambioArriba = false;
	 menu::p1CambioAbajo = false;
	 menu::p2CambioArriba = false;
	 menu::p2CambioAbajo = false;
	 menu::tamañoFuente = 30;
}
void menu::menuInput() {
	if (IsKeyDown(KEY_ZERO)) {
		GameLoop::pantallaActual = PANTALLAS::MENU;
	}
	else if (IsKeyPressed(KEY_ONE)) {
		GameLoop::pantallaActual = PANTALLAS::OPCJUGAR;
	}
	else if (IsKeyPressed(KEY_TWO)) {
		GameLoop::pantallaActual = PANTALLAS::INSTRUCCIONES;
	}
	else if (IsKeyPressed(KEY_THREE)) {
		GameLoop::pantallaActual = PANTALLAS::OPCIONES;
	}		
	else if (IsKeyPressed(KEY_FOUR)) {
		GameLoop::pantallaActual = PANTALLAS::CREDITOS;
	}
	else if (IsKeyPressed(KEY_FIVE)) {
		GameLoop::pantallaActual = PANTALLAS::SALIR;
	}	
}
void menu::menuJugarInput() {
    if (IsKeyPressed(KEY_ONE)) {
		GameLoop::pantallaActual = PANTALLAS::GAMEPLAY;
		gamePlay::IA = false;
	}
	if (IsKeyPressed(KEY_TWO)) {
		GameLoop::pantallaActual = PANTALLAS::GAMEPLAY;
		gamePlay::IA = true;
	}
	if (IsKeyPressed(KEY_THREE)) {
		GameLoop::pantallaActual = PANTALLAS::OPCIONESCOLOR;
	}
}
void menu::cambioTeclas(Player& p1, Player& p2) {
	int diferencia = 32;
	if (IsKeyDown((KeyboardKey)p1.abajo)) { menu::p1CambioAbajo = true; }
	if (menu::p1CambioAbajo) {
		int aux = GetKeyPressed() - diferencia;
		if (aux > 0) {
			p1.abajo = aux;
		}
		menu::p1CambioAbajo = false;
	}
	if (IsKeyDown((KeyboardKey)p1.arriba)) { menu::p1CambioArriba = true; }
	if (menu::p1CambioArriba) {
		int aux1 = GetKeyPressed() - diferencia;
		if (aux1 > 0) {
			p1.arriba = aux1;
		}
		menu::p1CambioArriba = false;
	}

	if (IsKeyDown((KeyboardKey)p2.abajo)) { menu::p2CambioAbajo = true; }
	if (menu::p2CambioAbajo) {
		int aux2 = GetKeyPressed() - diferencia;
		if (aux2 > 0) {
			p2.abajo = aux2;
		}
		menu::p2CambioAbajo = false;
	}
	if (IsKeyDown((KeyboardKey)p2.arriba)) { menu::p2CambioArriba = true; }
	if (menu::p2CambioArriba) {
		int aux3 = GetKeyPressed() - diferencia;
		if (aux3 > 0) {
			p2.arriba = aux3;
		}
		menu::p2CambioArriba = false;
	}
}

void menu::pantallaMenu() {
	menu::menuInput();	
}
void menu::pantallaJugar() {	
	menu::menuJugarInput();
	if (IsKeyDown(KEY_BACKSPACE)) { GameLoop::pantallaActual = PANTALLAS::MENU; }
}
void menu::pantallaInstrucciones() {
	if (IsKeyDown(KEY_BACKSPACE)) { GameLoop::pantallaActual = PANTALLAS::MENU; }
}
void menu::pantallaOpColor(Player& p1, Player& p2, Pelota& bola) {
	p1.pad.x = static_cast<float>(screenWidth) / 5.3f;
	p2.pad.x = static_cast<float>(screenWidth) / 1.332f;	
	menu::colorPad(p1,p2,bola);
	if (IsKeyDown(KEY_BACKSPACE)) { GameLoop::pantallaActual = PANTALLAS::OPCJUGAR; }
}
void menu::colorPad(Player& p1, Player& p2, Pelota& bola) {
	const short TAM = 10;
	Color tono[TAM] = { RED,GREEN,SKYBLUE,YELLOW,MAGENTA,LIME,WHITE,PINK,ORANGE,VIOLET };
	if (IsKeyPressed(KEY_SPACE)) {
		menu::i == TAM - 1 ? menu::i = 0: menu::i++;		
	}
	if (IsKeyPressed(KEY_ENTER)) { 
		menu::j == TAM - 1 ? menu::j = 0 : menu::j++;
	}
	if (IsKeyPressed(KEY_UP)) {
		menu::p == TAM - 1 ? menu::p = 0 : menu::p++;
	}
	p1.tono = tono[menu::i];
	p2.tono = tono[menu::j];
	bola.tono = tono[menu::p];
}
void menu::pantallaControles(Player& p1, Player& p2) {
	menu::cambioTeclas(p1, p2);
	if (IsKeyDown(KEY_BACKSPACE)) { GameLoop::pantallaActual = PANTALLAS::MENU;}
}
void menu::pantallaCreditos() {
	if (IsKeyDown(KEY_BACKSPACE)) { GameLoop::pantallaActual = PANTALLAS::MENU; }
}

void menu::dibPantallaMenu() {
	DrawText("PONG", static_cast<int>(screenWidth /2.667f) , screenHeight / 15, tamañoFuente+15, GREEN);
	DrawText("1. Jugar ", static_cast<int>(screenWidth / 2.667f), screenHeight / 3, tamañoFuente, GREEN);
	DrawText("2. Instrucciones ", static_cast<int>(screenWidth / 2.667f), static_cast<int>(screenHeight / 2.25f), tamañoFuente, GREEN);
	DrawText("3. Controles ", static_cast<int>(screenWidth / 2.667f), static_cast<int>(screenHeight / 1.8f), tamañoFuente, GREEN);
	DrawText("4. Creditos ", static_cast<int>(screenWidth / 2.667f), static_cast<int>(screenHeight / 1.5f), tamañoFuente, GREEN);
	DrawText("5. Salir", static_cast<int>(screenWidth / 2.667f), static_cast<int>(screenHeight / 1.3f), tamañoFuente, GREEN);
}
void menu::dibPantallaJugar() {
	DrawText("PONG", static_cast<int>(screenWidth / 2.667f), screenHeight / 15, tamañoFuente+15, GREEN);
	DrawText("1. Player Vs Player  ", static_cast<int>(screenWidth / 2.667f), screenHeight / 3, tamañoFuente, GREEN);
	DrawText("2. Player Vs CPU ", static_cast<int>(screenWidth / 2.667f), static_cast<int>(screenHeight / 2.25f), tamañoFuente, GREEN);
	DrawText("3. Elejir color Pad ", static_cast<int>(screenWidth / 2.667f), static_cast<int>(screenHeight / 1.8f), tamañoFuente, GREEN);
}
void menu::dibPantallaInstrucciones() {
	DrawText("jugador 1 ", screenWidth / 16, screenHeight / 18, tamañoFuente, GREEN);
	DrawText("mover arriba A ", screenWidth / 16, screenHeight / 9, tamañoFuente, RED);
	DrawText("mover abajo  D ", screenWidth / 16, screenHeight / 6, tamañoFuente, RED);

	DrawText("jugador 2 ", static_cast<int>(screenWidth / 1.778f), screenHeight / 18, tamañoFuente, GREEN);
	DrawText("mover arriba UP ", static_cast<int>(screenWidth / 1.778f), screenHeight / 9, tamañoFuente, BLUE);
	DrawText("mover abajo  DOWN ", static_cast<int>(screenWidth / 1.778f), screenHeight / 6, tamañoFuente, BLUE);

	DrawText("se gana con 6 puntos ", static_cast<int>(screenWidth / 4.0f), static_cast<int>(screenHeight / 4), tamañoFuente, GREEN);

	DrawText("  presiona SPACE para pausar ", static_cast<int>(screenWidth / 6.0f), static_cast<int>(screenHeight / 2.649f), tamañoFuente , GREEN);
	DrawText("presiona BACKSPACE para regresar", static_cast<int>(screenWidth / 8.0f), static_cast<int>(screenHeight / 2.146f), tamañoFuente , GREEN);

	DrawRectangle(screenWidth / 16, static_cast<int>(screenHeight / 1.225f), 10, 20, YELLOW);
	DrawText("Se activa powerup random por tiempo", screenWidth / 10, static_cast<int>(screenHeight / 1.225f), tamañoFuente, GREEN);
}
void menu::dibPantallaOpColor(Player& p1, Player& p2, Pelota& bola) {
	DrawText(" Player 1  ", screenWidth / 8, screenHeight / 9, tamañoFuente, GREEN);
	DrawText(" Player 2 ", static_cast<int>(screenWidth / 1.455f), screenHeight /9, tamañoFuente, GREEN);
	DrawRectangleRec(p1.pad, p1.tono);
	DrawRectangleRec(p2.pad, p2.tono);
	DrawCircleV(bola.pos, bola.radio, bola.tono);
	DrawText(" press UP ", static_cast<int>(screenWidth / 2.5f), static_cast<int>(screenHeight / 1.5), tamañoFuente, GREEN);
	DrawText(" press space ", screenWidth / 16, static_cast<int>(screenHeight / 1.225f), tamañoFuente, GREEN);
	DrawText(" press enter ", static_cast<int>(screenWidth / 1.6f), static_cast<int>(screenHeight / 1.225f), tamañoFuente, GREEN);
}
void menu::dibPantallaControles(Player& p1, Player& p2) {
	DrawText("jugador 1 ", screenWidth / 16, screenHeight / 18, tamañoFuente, RED);
	DrawText("mover arriba A ", screenWidth/ 16, screenHeight / 9, tamañoFuente, RED);
	DrawText("mover abajo  D ", screenWidth / 16, screenHeight / 6, tamañoFuente, RED);

	DrawText(" presiona la tecla que deseas ", static_cast<int>(screenWidth / 11.43f), static_cast<int>(screenHeight / 2.649f), tamañoFuente+10, RED);
	DrawText("cambiar y sin soltarla presiona", static_cast<int>(screenWidth / 11.43f), static_cast<int>(screenHeight / 2.146f), tamañoFuente+10, RED);
	DrawText("   presiona la nueva tecla  ", static_cast<int>(screenWidth / 11.43f), static_cast<int>(screenHeight / 1.8f), tamañoFuente+10, RED);

	DrawText("jugador 2 ", static_cast<int>(screenWidth / 1.778f), screenHeight / 18, tamañoFuente, BLUE);
	DrawText("mover arriba UP ", static_cast<int>(screenWidth / 1.778f), screenHeight / 9, tamañoFuente, BLUE);
	DrawText("mover abajo  DOWN ", static_cast<int>(screenWidth / 1.778f), screenHeight / 6, tamañoFuente, BLUE);
}
void menu::dibPantallaCreditos() {
	DrawText("CREDITOS ", static_cast<int>(screenWidth / 2.965f), static_cast<int>(screenHeight / 12.856f), tamañoFuente+20, BLUE);
	DrawText("Juan Sebastian Gomez ", static_cast<int>(screenWidth / 3.81f), static_cast<int>(screenHeight / 2.5f), tamañoFuente+5, BLUE);
}



