#ifndef MENU_H
#define MENU_H

#include "GamePlay/globales.h"

using namespace pong;
using namespace gamePlay;

namespace pong {

	namespace menu {

		void inicializarMenu();
		void menuInput();
		void menuJugarInput();
		void cambioTeclas(Player& p1, Player& p2);

		void pantallaMenu();
		void pantallaJugar();
		void pantallaInstrucciones();
		void pantallaOpColor(Player& p1, Player& p2, Pelota& bola);
		void colorPad(Player& p1, Player& p2, Pelota& bola);
		void pantallaControles(Player& p1, Player& p2);
		void pantallaCreditos();		
	
		void dibPantallaMenu();
		void dibPantallaJugar();
		void dibPantallaInstrucciones();
		void dibPantallaOpColor(Player& p1, Player& p2, Pelota& bola);
		void dibPantallaControles(Player& p1, Player& p2);
		void dibPantallaCreditos();		
	}
}
#endif